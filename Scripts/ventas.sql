
create table ventas(
  id int primary key auto_increment,
  vendedor varchar(100),
  modelo varchar(100),
  enero int,
  febrero int,
  marzo int,
  foreign key(vendedor) references vendedores(codigo),
  foreign key (modelo) references automovil(nombre)
);

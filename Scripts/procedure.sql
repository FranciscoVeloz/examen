drop procedure if exists p_InsertarVendedor;

create procedure p_InsertarVendedor(
    in p_codigo varchar(100),
    in p_nombre varchar(100)
)

p_InsertarVendedor : begin

    insert into vendedores values(p_codigo, p_nombre);
    select * from vendedores;

end;

call p_InsertarVendedor("H1Z1", "Francisco Veloz");

drop procedure if exists p_ActualizarVendedor;

create procedure p_ActualizarVendedor(
    in p_codigo varchar(100),
    in p_nombre varchar(100)
)

p_ActualizarVendedor : begin

    update vendedores set nombre = p_nombre where codigo = p_codigo;
    select * from vendedores;

end;

call p_InsertarVendedor("H1Z1", "FRANCISCO GLEZ VELOZ");

drop procedure if exists p_EliminarVendedor;

create procedure p_EliminarVendedor(
    in p_codigo varchar(100)
)

p_EliminarVendedor : begin

    delete from vendedores where codigo = p_codigo;
    select * from vendedores;

end;

call p_EliminarVendedor("H1Z1");



